import requests
from django.conf import settings

from django_mdat_location.django_mdat_location.models import (
    MdatHouseNumbers,
    MdatCountries,
    MdatCities,
    MdatStreets,
)


def mdat_google_find_address_by_place_id(place_id: str):
    place_response = requests.get(
        url="https://maps.googleapis.com/maps/api/place/details/json",
        params={
            "fields": "address_component,geometry",
            "place_id": place_id,
            "key": settings.GOOGLE_PLACES_API_KEY,
        },
    )

    if place_response.status_code != 200:
        return False

    place_data = place_response.json()

    if "status" not in place_data:
        return False

    if place_data["status"] != "OK":
        return False

    place_details = dict()

    for address_component in place_data["result"]["address_components"]:
        for component_type in address_component["types"]:
            place_details[component_type] = address_component

    if "route" not in place_details or "street_number" not in place_details:
        return False

    try:
        country = MdatCountries.objects.get(iso=place_details["country"]["short_name"])
    except MdatCountries.DoesNotExist:
        return False

    try:
        city = MdatCities.objects.get(
            country=country, zip_code=place_details["postal_code"]["short_name"]
        )
    except MdatCities.DoesNotExist:
        city = MdatCities(
            country=country,
            zip_code=place_details["postal_code"]["short_name"],
            title=place_details["locality"]["long_name"],
        )
        city.save()

    try:
        street = MdatStreets.objects.get(
            city=city, title=place_details["route"]["long_name"]
        )
    except MdatStreets.DoesNotExist:
        street = MdatStreets(
            city=city,
            title=place_details["route"]["long_name"],
        )
        street.save()

    try:
        address = MdatHouseNumbers.objects.get(
            street=street,
            house_nr=place_details["street_number"]["short_name"],
        )
    except MdatHouseNumbers.DoesNotExist:
        address = MdatHouseNumbers(
            street=street,
            house_nr=place_details["street_number"]["short_name"],
            google_place_id=place_id,
        )
        address.save()

    if address.geoloc is None and "geometry" in place_data["result"]:
        address.geoloc = {
            "coordinates": [
                place_data["result"]["geometry"]["location"]["lng"],
                place_data["result"]["geometry"]["location"]["lat"],
            ],
            "type": "Point",
        }

        address.save()

    return address


def mdat_google_find_address_by_string(
    search_string: str, return_all=False, place_id_only=False
):
    find_place_response = requests.get(
        url="https://maps.googleapis.com/maps/api/place/findplacefromtext/json",
        params={
            "fields": "place_id",
            "input": search_string,
            "inputtype": "textquery",
            "key": settings.GOOGLE_PLACES_API_KEY,
        },
    )

    if find_place_response.status_code != 200:
        return False

    find_place_data = find_place_response.json()

    if "candidates" not in find_place_data:
        return False

    place_results = list()

    for place in find_place_data["candidates"]:
        place_id = place["place_id"]

        if place_id_only:
            if not return_all:
                return place_id

            place_results.append(place_id)
        else:
            address = mdat_google_find_address_by_place_id(place_id)

            if address is not False:
                if return_all is False:
                    return address

                place_results.append(address)

    if len(place_results) <= 0:
        return False

    return place_results


def mdat_google_places_distance(from_address_place_id: str, to_address_place_id: str):
    distance_response = requests.get(
        url="https://maps.googleapis.com/maps/api/distancematrix/json",
        params={
            "origins": f"place_id:{from_address_place_id}",
            "destinations": f"place_id:{to_address_place_id}",
            "mode": "driving",
            "units": "metric",
            "key": settings.GOOGLE_PLACES_API_KEY,
        },
    )

    if distance_response.status_code != 200:
        return False

    distance_data = distance_response.json()

    if "rows" in distance_data:
        first_row = distance_data["rows"][0]

        if "elements" in first_row:
            first_element = first_row["elements"][0]

            return first_element

    return False
