from django.core.management.base import BaseCommand

from django_mdat_location_tasks.django_mdat_location_tasks.tasks import (
    mdat_location_seed_google_place_id,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        mdat_location_seed_google_place_id()
