from django.core.management.base import BaseCommand

from django_mdat_location_tasks.django_mdat_location_tasks.tasks import (
    mdat_location_sync_from_google_dspl_country_codes,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write(self.help)

        mdat_location_sync_from_google_dspl_country_codes()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
