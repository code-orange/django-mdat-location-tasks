import csv
from io import StringIO

import requests
from celery import shared_task
from datapackage import Package
from django.apps import apps

from django_mdat_customer.django_mdat_customer.models import MdatCustomers
from django_mdat_location.django_mdat_location.models import *
from django_mdat_location_tasks.django_mdat_location_tasks.func import (
    mdat_google_find_address_by_string,
    mdat_google_places_distance,
)


@shared_task(name="mdat_location_sync_from_datahub_country_codes")
def mdat_location_sync_from_datahub_country_codes():
    package = Package("https://datahub.io/core/country-codes/datapackage.json")

    # print processed tabular data (if exists any)
    for resource in package.resources:
        if resource.descriptor["datahub"]["type"] == "derived/csv":
            data = resource.read(keyed=True)

            for row in data:
                # Check if ISO code is present
                if (
                    "ISO3166-1-Alpha-2" not in row
                    or "ISO3166-1-Alpha-3" not in row
                    or "ISO3166-1-numeric" not in row
                ):
                    continue

                if (
                    row["ISO3166-1-Alpha-2"] == ""
                    or row["ISO3166-1-Alpha-2"] is None
                    or row["ISO3166-1-Alpha-3"] == ""
                    or row["ISO3166-1-Alpha-3"] is None
                    or row["ISO3166-1-numeric"] == ""
                    or row["ISO3166-1-numeric"] is None
                ):
                    continue

                try:
                    country = MdatCountries.objects.get(
                        num_code=int(row["ISO3166-1-numeric"])
                    )
                except MdatCountries.DoesNotExist:
                    try:
                        country = MdatCountries.objects.get(
                            iso3=row["ISO3166-1-Alpha-3"]
                        )
                    except MdatCountries.DoesNotExist:
                        try:
                            country = MdatCountries.objects.get(
                                iso=row["ISO3166-1-Alpha-2"]
                            )
                        except MdatCountries.DoesNotExist:
                            country = MdatCountries()

                country.num_code = int(row["ISO3166-1-numeric"])
                country.iso3 = row["ISO3166-1-Alpha-3"]
                country.iso = row["ISO3166-1-Alpha-2"]

                # Import translations
                country.set_current_language("en")
                try:
                    title = country.title
                except:
                    country.title = row["official_name_en"]

                country.set_current_language("de")
                try:
                    title = country.title
                except:
                    country.title = row["official_name_en"]

                country.save()

    return


@shared_task(name="mdat_location_sync_from_google_dspl_country_codes")
def mdat_location_sync_from_google_dspl_country_codes():
    source = "https://raw.githubusercontent.com/google/dspl/master/samples/google/canonical/countries.csv"
    response = requests.get(source).text
    response_file = StringIO(response)

    csv_reader = csv.DictReader(response_file, delimiter=",")

    for row in csv_reader:
        try:
            country = MdatCountries.objects.get(iso=row["country"])
        except MdatCountries.DoesNotExist:
            country = MdatCountries(iso=row["country"])
            country.save()

        country.geoloc = {
            "coordinates": (str(row["longitude"]), str(row["latitude"])),
            "type": "Point",
        }

        # Import translations
        country.set_current_language("en")
        try:
            title = country.title
        except:
            country.title = row["name"]

        country.set_current_language("de")
        try:
            title = country.title
        except:
            country.title = row["name"]

        country.save()

    return


@shared_task(name="mdat_location_seed_google_place_id")
def mdat_location_seed_google_place_id():
    model_list = [
        "MdatCountries",
        "MdatCities",
        "MdatStreets",
        "MdatHouseNumbers",
    ]

    for model in model_list:
        model_class = apps.get_model("django_mdat_location", model)

        for location in model_class.objects.filter(google_place_id__isnull=True):
            print("UPDATE: {} - {}".format(model, location.__str__()))

            place_id = mdat_google_find_address_by_string(
                search_string=location.__str__(),
                return_all=False,
                place_id_only=True,
            )

            if place_id:
                location.google_place_id = place_id

            location.save()

    return


@shared_task(name="mdat_location_seed_distance_for_customers")
def mdat_location_seed_distance_for_customers():
    for customer in list(MdatCustomers.objects.all()):
        print(customer.id)

        try:
            distance = MdatHouseNumberDistances.objects.get(
                from_address_id=customer.created_by.mdatcustomerdeliveryaddresses.address.id,
                to_address_id=customer.mdatcustomerdeliveryaddresses.address.id,
            )
        except MdatHouseNumberDistances.DoesNotExist:
            zero_results = False
            drive_distance_meters = 0
            drive_distance_seconds = 0

            distance_data = mdat_google_places_distance(
                customer.created_by.mdatcustomerdeliveryaddresses.address.google_place_id,
                customer.mdatcustomerdeliveryaddresses.address.google_place_id,
            )

            if distance_data["status"] == "OK":
                drive_distance_meters = distance_data["distance"]["value"]
                drive_distance_seconds = distance_data["duration"]["value"]

            MdatHouseNumberDistances.objects.create(
                from_address_id=customer.created_by.mdatcustomerdeliveryaddresses.address.id,
                to_address_id=customer.mdatcustomerdeliveryaddresses.address.id,
                drive_distance_meters=drive_distance_meters,
                drive_distance_seconds=drive_distance_seconds,
            )

    return
