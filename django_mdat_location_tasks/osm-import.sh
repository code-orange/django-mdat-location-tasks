#!/usr/bin/env bash

set -x
set -e

wget https://download.geofabrik.de/europe/germany-latest.osm.pbf -O /tmp/germany-latest.osm.pbf
osmconvert /tmp/germany-latest.osm.pbf -o=/tmp/germany-latest.osm.o5m
osmfilter /tmp/germany-latest.osm.o5m --keep="addr:country= and addr:city= and addr:street=" --ignore-depemdencies --drop-relations --drop-ways | osmconvert - --csv="@oname @id @lon @lat addr:country addr:city addr:street" > germany-latest.csv
